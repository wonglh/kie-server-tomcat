Drools KIE Server on Tomcat Docker image
=======================================

Drools KIE Server on Tomcat Docker image.

```
docker run -d --name redis-session-store redis
```


```
docker run -p 8180:8080 -d --link redis-session-store:redis --name kie-server-tomcat lhwong/kie-server-tomcat

```
More information of configuring KIE Server for Tomcat available at http://mswiderski.blogspot.my/2015/10/installing-kie-server-and-workbench-on.html